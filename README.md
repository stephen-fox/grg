# grg
grg (Go RPC Generator) provides functionality for auto-generating 'net/rpc'
library API bindings.

This is particularly useful for generating RPC bindings for existing Go APIs.

## Features

- Easily generate RPC bindings for a single function via command line
  arguments, or for several by passing their signatures to stdin
- Skips including `error` types in the resulting output structs
- Customizable RPC server struct name and input / output struct suffixes

## Usage
```
grg [options] [existing function signature]
```

## Examples

#### Common usage
Let's pretend we would like to implement a RPC server that calls `os.Setenv`
(among possibly other functions). If you would like to quickly create a binding
for this single function, pass the function signature to `grg` (be aware you
will likely need to put the  string in quotes because your shell may try to
expand characters from the string):

```sh
# Warning: You will likely need to put the string in quotes because your shell
# may try to expand characters from it. Note the quotes below.
grg 'Setenv(key string, value string) error'
```

This will produce the output:
```go
type SetenvIn struct {
	Key string
	Value string
}

type SetenvOut struct {}

func (o *TODOServer) Setenv(*SetenvIn, *SetenvOut) error {
	return errors.New("TODO: not implemented yet")
}
```

#### Customizing RPC server struct name and other names
If you have an existing RPC server data structure that you would like the
auto-generated code to reference, simply specify the `-struct` option:


```sh
grg -struct OSServer 'Setenv(key string, value string) error'
```

Which will produce:
```go
type SetenvIn struct {
	Key string
	Value string
}

type SetenvOut struct {}

func (o *OSServer) Setenv(*SetenvIn, *SetenvOut) error {
	return errors.New("TODO: not implemented yet")
}
```

You can also customize the input and output object suffixes using the
`-isuffix` and `-osuffix` options:

```sh
grg -isuffix Input -osuffix Output 'Setenv(key string, value string) error'
```

Which will produce:
```go
type SetenvInput struct {
	Key string
	Value string
}

type SetenvOutput struct {}

func (o *TODOServer) Setenv(*SetenvInput, *SetenvOutput) error {
	return errors.New("TODO: not implemented yet")
}
```

#### Creating bindings for multiple APIs
Previously, we generated RPC bindings for the `os.Setenv` function. This time,
let's  imagine that we would like to generate bindings for several
`os` functions:

```sh
grg
# Paste the following data followed by a new line. Close stdin with 'Control+D'
# on Unix systems, or on Windows with 'Control+C' (interrupt).
Setenv(key string, value string)
Environ() []string
Getenv(key string) string
LookupEnv(key string) (string, bool)

```

This will generate the following RPC bindings:

```go
type SetenvIn struct {
	Key string
	Value string
}

type SetenvOut struct {}

func (o *TODOServer) Setenv(*SetenvIn, *SetenvOut) error {
	return errors.New("TODO: not implemented yet")
}

type EnvironIn struct {}

type EnvironOut struct {
	Strings []string
}

func (o *TODOServer) Environ(*EnvironIn, *EnvironOut) error {
	return errors.New("TODO: not implemented yet")
}

type GetenvIn struct {
	Key string
}

type GetenvOut struct {
	String string
}

func (o *TODOServer) Getenv(*GetenvIn, *GetenvOut) error {
	return errors.New("TODO: not implemented yet")
}

type LookupEnvIn struct {
	Key string
}

type LookupEnvOut struct {
	String string
	Bool bool
}

func (o *TODOServer) LookupEnv(*LookupEnvIn, *LookupEnvOut) error {
	return errors.New("TODO: not implemented yet")
}
```

## What is RPC, and Go `net/rpc`?
The [`net/rpc`](https://golang.org/pkg/net/rpc/) Go library is part of Go's
standard library. RPC stands for "Remote Procedure Call", or the process of
invoking a procedure in another running software process.

The Go RPC library helps developers write an application that needs to be split
across IPC (interprocess communication) boundaries. One of the most common
examples of this (not specific to RPC) is an HTTP server and client. The server
is really just exposing memory and storage to the HTTP client by serializing
data, possibly also providing authentication, authorization, and transport
layer security (TLS).

RPC libraries generally try to make the process of serializing and
deserializing data as intuitive and reliable as possible. Implementers still
need to consider a security model, as in Go's case the library does not
auto-magically know how to solve trust, transport encryption, and other
security problems for you. The `net/rpc` library is a bit maclunky to use,
but it gets the job done.
