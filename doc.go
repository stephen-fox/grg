// Package grg (Go RPC Generator) provides functionality for auto-generating
// 'net/rpc' library API bindings.
package grg
