package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"unicode"
)

const (
	appName = "grg"

	serverStructNameArg = "struct"
	inputSuffixArg      = "isuffix"
	outputSuffixArg     = "osuffix"
	optionalPrefixArg   = "prefix"
	helpArg             = "h"

	usage = appName + `

Generates Go RPC bindings for existing Go APIs (Go RPC Generator).

usage: ` + appName + ` [options] [go function-signature]

options:
`
)

func main() {
	serverStructName := flag.String(serverStructNameArg, "TODOServer", "The RPC server struct name")
	inputTSuffix := flag.String(inputSuffixArg, "In", "The input type suffix to use")
	outputTSuffix := flag.String(outputSuffixArg, "Out", "The output type suffix to use")
	optionalPrefix := flag.String(optionalPrefixArg, "", "Prefix functions and types with this value")
	help := flag.Bool(helpArg, false, "Display this information")

	flag.Parse()

	if *help {
		os.Stderr.WriteString(usage)
		flag.PrintDefaults()
		os.Exit(1)
	}

	baseConfig := parseFunctionConfig{
		ServerStruct:   *serverStructName,
		FuncStr:        strings.TrimSpace(strings.Join(flag.Args(), " ")),
		InputTSuffix:   *inputTSuffix,
		OutputTSuffix:  *outputTSuffix,
		OptionalPrefix: *optionalPrefix,
	}

	if flag.NArg() > 0 {
		baseConfig.FuncStr = strings.TrimSpace(strings.Join(flag.Args(), " "))
		result, err := parseFunction(baseConfig)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Printf("%s\n", result)
		return
	}

	if runtime.GOOS == "windows" {
		// Windows sucks and does not support closing stdin
		// via signal / keyboard combo.
		onKill := make(chan os.Signal, 1)
		signal.Notify(onKill, os.Interrupt, os.Kill)
		defer signal.Stop(onKill)
		go func() {
			<-onKill
			os.Stdin.Close()
		}()
	}

	raw, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatalln(err)
	}

	scanner := bufio.NewScanner(bytes.NewReader(raw))
	for scanner.Scan() {
		fnStr := strings.TrimSpace(scanner.Text())
		if len(fnStr) == 0 || strings.HasPrefix(fnStr, "//") || strings.HasPrefix(fnStr, "#") {
			continue
		}

		baseConfig.FuncStr = fnStr
		result, err := parseFunction(baseConfig)
		if err != nil {
			log.Fatalf("failed to parse function '%s' - %s", fnStr, err)
		}
		fmt.Printf("\n%s\n", result)
	}
}

type parseFunctionConfig struct {
	ServerStruct   string
	FuncStr        string
	InputTSuffix   string
	OutputTSuffix  string
	OptionalPrefix string
}

func parseFunction(config parseFunctionConfig) (string, error) {
	str := config.FuncStr

	argsStartIndex := strings.Index(str, "(")
	if argsStartIndex < 0 {
		return "", errors.New("missing args start")
	}

	if argsStartIndex == 0 {
		return "", errors.New("missing function name")
	}

	afterArgsStart := str[argsStartIndex+1:]
	argsEndIndex := strings.Index(afterArgsStart, ")")
	if argsEndIndex < 0 {
		return "", errors.New("missing args end")
	}

	functionName := config.OptionalPrefix + str[0:argsStartIndex]
	sb := &strings.Builder{}

	inputTypeName := startRPCFunctionArgType(rpcFunctionArgConfig{
		OrigFuncName: functionName,
		Builder:      sb,
		TypeSuffix:   config.InputTSuffix,
	})
	if argsEndIndex > 0 {
		addMultipleFunctionArgs(afterArgsStart[0:argsEndIndex], sb, true)
	}
	sb.WriteString("}\n\n")

	outputTypeName := startRPCFunctionArgType(rpcFunctionArgConfig{
		OrigFuncName: functionName,
		Builder:      sb,
		TypeSuffix:   config.OutputTSuffix,
	})
	err := addReturnTypes(afterArgsStart[argsEndIndex+1:], sb)
	if err != nil {
		return "", err
	}
	sb.WriteString("}\n\n")

	sb.WriteString("func (o *")
	sb.WriteString(config.ServerStruct)
	sb.WriteString(") ")
	sb.WriteString(functionName)
	sb.WriteString("(*")
	sb.WriteString(inputTypeName)
	sb.WriteString(", *")
	sb.WriteString(outputTypeName)
	sb.WriteString(") error {\n\t")
	sb.WriteString(`return errors.New("TODO: not implemented yet")`)
	sb.WriteString("\n}")

	return sb.String(), nil
}

func addMultipleFunctionArgs(argsStr string, sb *strings.Builder, includeErrType bool) {
	sb.WriteByte('\n')
	argStrs := strings.Split(argsStr, ",")
	for _, argStr := range argStrs {
		parsed := parseFunctionArgStr(argStr)
		if !includeErrType && parsed.Type == "error" {
			continue
		}
		sb.WriteByte('\t')
		sb.WriteString(parsed.MustName())
		sb.WriteByte(' ')
		sb.WriteString(parsed.Type)
		sb.WriteByte('\n')
	}
}

type rpcFunctionArgConfig struct {
	OrigFuncName string
	Builder      *strings.Builder
	TypeSuffix   string
}

func startRPCFunctionArgType(config rpcFunctionArgConfig) string {
	config.Builder.WriteString("type ")

	tName := string(config.OrigFuncName[0])
	if len(config.OrigFuncName) > 1 {
		tName = tName + config.OrigFuncName[1:]
	}

	tName = tName + config.TypeSuffix
	config.Builder.WriteString(tName)
	config.Builder.WriteString(" struct {")
	return tName
}

func addReturnTypes(onlyReturnInfo string, sb *strings.Builder) error {
	onlyReturnInfo = strings.TrimSpace(onlyReturnInfo)
	if len(onlyReturnInfo) == 0 {
		// void return type.
		return nil
	}

	retValsStartIndex := strings.Index(onlyReturnInfo, "(")
	if retValsStartIndex < 0 {
		arg := parseFunctionArgStr(onlyReturnInfo)
		if arg.Type == "error" {
			// only returns 'error' type.
			return nil
		}
		sb.WriteString("\n\t")
		sb.WriteString(arg.MustName())
		sb.WriteString(" ")
		sb.WriteString(arg.Type)
		sb.WriteString("\n")
		// returns a single, non-error type.
		return nil
	}

	retValsEndIndex := strings.Index(onlyReturnInfo, ")")
	if retValsEndIndex < 0 {
		return errors.New("missing return values closing token")
	}

	addMultipleFunctionArgs(onlyReturnInfo[retValsStartIndex+1:retValsEndIndex], sb, false)
	return nil
}

func parseFunctionArgStr(str string) functionArg {
	str = strings.TrimSpace(str)
	sepInddex := strings.Index(str, " ")
	if sepInddex > 0 {
		return functionArg{
			Name: str[0:sepInddex],
			Type: str[sepInddex+1:],
		}
	}

	return functionArg{
		Name: "",
		Type: str,
	}
}

type functionArg struct {
	Name string
	Type string
}

func (o functionArg) MustName() string {
	if len(o.Name) > 0 {
		return upperCaseFirstLetterInStr(o.Name)
	}

	var typeString string
	nsSepIndex := strings.Index(o.Type, ".")
	if nsSepIndex > 0 {
		typeString = o.Type[nsSepIndex+1:]
	} else {
		typeString = o.Type
	}

	if len(typeString) == 1 {
		return strings.ToUpper(typeString)
	}

	if strings.HasPrefix(typeString, "[]") {
		typeString = strings.TrimPrefix(typeString, "[]") + "s"
	} else if strings.HasPrefix(typeString, "map[") {
		endKeyIndex := strings.LastIndex(typeString, "]")
		if endKeyIndex < 0 {
			typeString = strings.TrimPrefix(typeString, "map[")
		} else {
			keyType := upperCaseFirstLetterInStr(typeString[4:endKeyIndex])
			valType := upperCaseFirstLetterInStr(typeString[endKeyIndex+1:])
			typeString = fmt.Sprintf("Map%sTo%s", keyType, valType)
		}
		return typeString
	}

	return upperCaseFirstLetterInStr(typeString)
}

func upperCaseFirstLetterInStr(str string) string {
	switch len(str) {
	case 0:
		return str
	case 1:
		return strings.ToUpper(str)
	default:
		if unicode.IsUpper(rune(str[0])) {
			return str
		}
		return strings.ToUpper(string(str[0])) + str[1:]
	}
}
